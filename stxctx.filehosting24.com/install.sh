#!/bin/bash
#########################################
#Installer stxctx.filehosting24.com	#
#Autor: Przemysław Jagielski	   	#
#Przeznaczenie dla storage z convert.	#
#########################################

#################
#Zmienne skryptu#
#################

folinst=/usr/src/dev.filehosting24.com/stxctx.filehosting24.com
folpinst=/usr/src
kernelversion=4.7.10

#####################
#Instalator właściwy#
#####################

clear
echo "Rozpoczynam instalacje, proszę czekać!"
echo -n "Podaj nazwę serwera storage:"
	read server

#####################
#Aktualizacja paczek#
#####################

cat $folinst/config/etc/apt/sources.list > /etc/apt/sources.list
cd /tmp
	wget https://www.dotdeb.org/dotdeb.gpg
	apt-key add dotdeb.gpg
	rm dotdeb.gpg
	cd /root
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install -y kernel-package build-essential ncurses-dev xz-utils binutils curl nginx-full php7.0-apcu php7.0-bz2 php7.0-cgi php7.0-cli php7.0-common php7.0-curl php7.0-fpm php7.0-gd php7.0-geoip \
	php7.0-imap php7.0-intl php7.0-json php7.0-mcrypt php7.0-mbstring php7.0-mysql php7.0-readline php7.0-ssh2 php7.0-xml php7.0-xmlrpc php7.0-xsl php7.0-zip \
	autoconf automake libass-dev libfreetype6-dev libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texinfo zlib1g-dev \
	yasm libx264-dev cmake mercurial libfdk-aac-dev libmp3lame-dev libopus-dev
apt-get install -y certbot -t jessie-backports

#########################################################
#Komplilacja ffmpeg wraz z pakietami spoza repozytorium.#
#########################################################

mkdir $folpinst/ffmpeg
cd $folpinst/ffmpeg
	hg clone https://bitbucket.org/multicoreware/x265
	cd x265/build/linux
		"PATH="$HOME/bin:$PATH" cmake -G "Unix makefiles" -DCmake_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED:bool=off ../../source" -j 4
		make -j 4
		make install
		make distclean
		cd ../../..
	wget http://storage.googleapis.com/downloads.webmproject.org/releases/webm/libvpx-1.5.0.tar.bz2
	tar xvf libvpx-1.5.0.tar.bz2
	cd libvpx-1.5.0
		"PATH="$HOME/bin:$PATH" ./configure --prefix="$HOME/ffmpeg_build" --disable-examples --disable-unit-tests"
		"PATH="$HOME/bin:$PATH"" make -j 4
		make install
		make clean
		cd ../
	wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
	tar xvf ffmpeg-snapshot.tar.bz2
	cd ffmpeg
		PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
			--prefix="$HOME/ffmpeg_build" \
			--pkg-config-flags="--static" \
			--extra-cflags="-I$HOME/ffmpeg_build/include" \
			--extra-ldflags="-L$HOME/ffmpeg_build/lib" \
			--bindir="$HOME/bin" \
			--enable-gpl \
			--enable-libass \
			--enable-libfdk-aac \
			--enable-libfreetype \
			--enable-libmp3lame \
			--enable-libopus \
			--enable-libtheora \
			--enable-libvorbis \
			--enable-libvpx \
			--enable-libx264 \
			--enable-libx265 \
			--enable-nonfree
		PATH="$HOME/bin:$PATH" make -j 4
		make install
		make distclean
		hash -r
		cd /root

####################
#Kompilacja kernela#
####################

mkdir $folpinst/kernel
cd $folpinst/kernel
	wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$kernelversion.tar.xz
	wget https://grsecurity.net/test/grsecurity-3.1-4.7.10-201610222037.patch
	tar xvf kernel-$kernelversion.tar.xz
	cp $folinst/config/usr/src/kernel/kernel.config $folpinst/kernel/kernel-$kernelversion
	cd $folpinst/kernel
		make menuconfig
		make-kpkg clean
		make-kpkg --initrd --revision=1.0-stct kernel_image -j 4
		cd ../
	dpkg -i kernel*.deb

#####################################
#Konfiguracja serwerów FTP, WWW itp.#
#####################################

	cd /etc/nginx
	service nginx stop
	service php7.0-fpm stop
	mv nginx.conf nginx.conf.old
	cp $folinst/config/etc/nginx/nginx.conf nginx.conf
	cd sites-available
		mv default default.old
		cp $folinst/etc/nginx/sites-available/default default
		sed -e 's/stxctx.filehosting24.com/'$server'/g' default
		ln -s default /etc/nginx/sites-enabled/default
		cd ../
	mkdir ssl
	cd ssl/
		openssl dhparam -out dhparam.pem 4096
		cd /etc/php/7.0/fpm
	mv php.ini php.ini.old
	cp $folinst/config/etc/php/7.0/fpm/php.ini php.ini
	cd pool.d
		mv www.conf www.conf.old
		cp $folinst/config/etc/php/7.0/fpm/pool.d/www.conf www.conf
		cd /home/
	mkdir -p www/$server
	chown -R www-data:www-data www/
	cd www/$server
		echo "<?php phpinfo(); ?>" > index.php
	certbot certonly --standalone -d $server -d www.$server
	certbot renew

service nginx start
service php7.0-fpm start

#####################
#Zakończenie skryptu#
#####################

echo "Instalacja zakończnona, usuń wszystkie foldery i źródła!"
