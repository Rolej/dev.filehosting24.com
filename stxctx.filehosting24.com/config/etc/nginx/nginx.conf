user www-data;
worker_processes auto;
pid /var/run/nginx.pid;

events {
	worker_connections 1024;
	multi_accept on;
	use epoll;
}

http {
        #######################
        #Ustawienia podstawowe#
        #######################

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        include /etc/nginx/mime.types;
        default_type text/html;
        access_log off;
        error_log /var/log/nginx/error.log;
        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
	client_max_body_size 10G;


        ############################
        #Ustawienia optymalizacyjne#
        ############################

        keepalive_timeout 10;
        types_hash_max_size 2048;
        client_body_timeout 60;
        client_header_timeout 60;
        send_timeout 60;
	fastcgi_read_timeout 300;

        ################
        #Kompresja gZip#
        ################

        gzip on;
        gzip_disable "msie6";
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 4;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
        #gzip_static on;

        ########################
        #Zabezpieczenie serwera#
        ########################

        server_tokens off;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;
        ssl_session_cache shared:SSL:50m;
        ssl_session_timeout 5m;
        ssl_dhparam /etc/nginx/ssl/dhparam.pem;
        ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
        add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

        ##################
        #Pamięć podręczna#
        ##################

        open_file_cache max=1000 inactive=20s;
        open_file_cache_valid 30s;
        open_file_cache_min_uses 2;
        open_file_cache_errors on;



}
